from setuptools import setup
from src.translator import __version__

with open('README.md', 'r') as file:
    desc = file.read()

setup(
    name="iqrf-gateway-translator",
    version=__version__,
    author="Karel Hanák",
    author_email="karel.hanak@iqrf.org",
    url="https://gitlab.iqrf.org/gateway/iqrf-gateway-translator",
    package_dir={"": "src"},
    packages=[
        "translator",
        "translator.cmqtt",
        "translator.crest",
        "translator.utils"
    ],
    package_data={
      "": ["schema_*.json"]
    },
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "iqrfgt=translator.__main__:main"
        ]
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Topic :: Communications"
    ],
    license="Apache License 2.0",
    install_requires=[
        "jsonschema >= 3.2.0",
        "paho-mqtt >= 1.5.0",
        "requests >= 2.22.0"
    ]
)
