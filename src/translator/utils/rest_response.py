"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


class RestResponse:  # pylint: disable=R0903
    """
    A class representing response from REST API.

    Attributes
    ----------
    body: str
        payload of the REST API response
    headers: dict
        headers of the REST API response
    status: int
        status code of the REST API response

    Methods
    -------
    jsonify()
        Returns response as json object.
    """

    def __init__(self, headers, body, status):
        """
        @param headers REST API response headers
        @param body REST API response payload
        @param status REST API response status code
        """
        self.headers = headers
        self.body = body
        self.status = status

    def jsonify(self):
        """
        Returns a json object containing headers, status code and payload of the REST API response.

        @return dictionary containing REST API response data
        """
        json = {
            'status': self.status,
            'headers': repr(self.headers),
            'body': self.body
        }
        return json
