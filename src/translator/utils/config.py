"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import json
import logging
import sys
from jsonschema import Draft4Validator
from translator.utils.error_codes import ErrorCodes


class Config:
    """
    A class implementing configuration.

    Attributes
    ----------
    config: str
        path to configuration file
    logger: logger
        logger instance for the Config object
    schema: str
        path to json schema file for validation

    Methods
    -------
    openAndLoad(path)
        Loads json file content.
    read()
        Loads and validates Translator configuration.
    """
    def __init__(self, file_path, schema_path):
        """
        @param file_path path to translator configuration file
        @param schema_path path to json schema file
        """
        self.config = file_path
        self.logger = logging.getLogger(__name__)
        self.schema = schema_path

    @staticmethod
    def open_and_load(path):
        """
        Loads content of a json file.

        @param path path to json file
        """
        with open(path, encoding='utf-8') as file:
            data = json.load(file)
        return data

    def read(self):
        """
        Loads json files containing configuration and json schema for validation.
        The configuration file is then validated against the schema.

        @return dictionary with translator configuration, JSON_ERROR if configuration file is not valid json file,
                SCHEMA_ERROR if configuration is invalid, IO_ERROR if an IO_ERRORor occurs
        """
        try:
            config = self.open_and_load(self.config)
            self.logger.info('Configuration file successfully loaded.')

            schema = self.open_and_load(self.schema)
            self.logger.info('Json schema file successfully loaded.')

            validator = Draft4Validator(schema)
            schema_errors = sorted(validator.iter_errors(config), key=str)

            if schema_errors:
                for error in schema_errors:
                    self.logger.error(error)
                return ErrorCodes.SCHEMA_ERROR, None

            self.logger.info('Configuration file successfully validated.')
            return ErrorCodes.OK, config
        except IOError as io_error:
            print('Failed to open file ' + io_error.filename, file=sys.stderr)
            self.logger.error('%s on file %s', io_error.strerror, io_error.filename)
            return ErrorCodes.IO_ERROR, None
        except json.JSONDecodeError as json_error:
            print('Failed to load json file due to invalid json file content.', file=sys.stderr)
            self.logger.error(json_error.msg + ' at line ' + str(json_error.lineno) +
                              ', column ' + str(json_error.colno) + ' on\n' + json_error.doc)
            return ErrorCodes.JSON_ERROR, None
