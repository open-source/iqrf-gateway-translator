"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import unittest
from translator.utils.rest_response import RestResponse


test_json = {
    'status': 200,
    'headers': "{'test-header': 'test-header-content'}",
    'body': 'test data'
}


class TestRestResponse(unittest.TestCase):

    def test_jsonify(self):
        resp = RestResponse({'test-header': 'test-header-content'}, 'test data', 200)
        self.assertEqual(resp.jsonify(), test_json)


if __name__ == '__main__':
    unittest.main()
