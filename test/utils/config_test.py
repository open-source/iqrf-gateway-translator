"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import pathlib
import unittest
from translator.utils.config import Config
from translator.utils.error_codes import ErrorCodes

test_json = {
    "rest": {
        'api_key': 'testapikey=',
        'addr': 'localhost',
        'port': 8080,
    },
    "mqtt": {
        'cid': 'test',
        'addr': 'localhost',
        'port': 1883,
        'request_topic': 'reqtopic/',
        'response_topic': 'resptopic/',
        'user': '',
        'pw': '',
        'tls': {
            'enabled': False,
            'trust_store': '',
            'key_store': '../../mqtt_client/data/example.pem',
            'private_key': '',
            'require_broker_certificate': True
        }
    }
}


class TestConfig(unittest.TestCase):

    def setUp(self):
        self.config_path = pathlib.Path(__file__).parent / 'data/test_config.json'
        self.config_error_path = pathlib.Path(__file__).parent / 'data/test_config_error.json'
        self.config_invalid_path = pathlib.Path(__file__).parent / 'data/test_config_invalid.json'
        self.schema_path = pathlib.Path(__file__).parent / 'data/test_schema_config.json'

    def test_read_success(self):
        config = Config(self.config_path, self.schema_path)

        self.assertEqual(config.read(), (ErrorCodes.OK, test_json))

    def test_read_invalid(self):
        config = Config(self.config_invalid_path, self.schema_path)

        self.assertEqual(config.read(), (ErrorCodes.SCHEMA_ERROR, None))

    def test_read_IO_ERROR(self):
        config = Config('nonexistant_file.json', self.schema_path)

        self.assertEqual(config.read(), (ErrorCodes.IO_ERROR, None))

    def test_read_JSON_ERROR(self):
        config = Config(self.config_error_path, self.schema_path)

        self.assertEqual(config.read(), (ErrorCodes.JSON_ERROR, None))


if __name__ == '__main__':
    unittest.main()
