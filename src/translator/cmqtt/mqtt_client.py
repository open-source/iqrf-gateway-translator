"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import json
import logging
import pathlib
import re
import ssl
from os import path
import paho.mqtt.client as mqtt
from jsonschema import Draft4Validator
from requests.exceptions import RequestException
from translator.utils.error_codes import ErrorCodes
from translator.utils.exceptions import TlsFileNotFoundError

dir_path = pathlib.Path(__file__).parent.parent


class CMqtt(mqtt.Client):
    """
    A class implementing behaviour of the Translator's MQTT client.

    Attributes
    ----------
    addr: str
        MQTT broker address
    logger: logger
        logger instance for the cmqtt object
    port: int
        MQTT broker port
    rest_client: crest
        rest_client class instance
    topic: str
        MQTT subscribe topic
    last_topic: str
        last used MQTT publish topic

    Callbacks
    ---------
    on_connect(client, userdata, flags, rc)
        Callback function for mqtt.client connect method.
    on_message(client, userdata, msg)
        Callback function for listening on subscribed topic.
    on_publish(client, userdata, mid)
        Callback function for mqtt.client publish method.
    on_subscribe(client, userdata, mid, qos)
        Callback function for mqtt.client subscribe method

    Methods
    -------
    validateRequest(request)
        Validates request published on MQTT subscribe topic.
    setup(config, rest_client)
        Initializes the MQTT client.
    """
    addr = ''
    endpoint = ''
    logger = logging.getLogger(__name__)
    port = ''
    rest_client = None
    request_topic = ''
    response_topic = ''
    validator = None

    @staticmethod
    def load_schema():
        """
        Loads request schema and stores validator

        @return Validator request json validator
        """
        with open(dir_path / 'utils/schema_request.json', encoding='utf-8') as file:
            schema_data = json.load(file)
        return Draft4Validator(schema_data)

    def _check_tls_file(self, file_path):
        if not file_path:
            return None
        if not path.exists(file_path):
            self.logger.error('Provided file %s does not exist.', file_path)
            raise TlsFileNotFoundError
        return file_path

    def _extract_endpoint(self, topic):
        """
        Extracts endpoint from request topic

        @return Endpoint
        """
        return topic[len(self.request_topic[:-3]):]

    def _validate_request(self, request):
        """
        Validates message published on MQTT subscribe topic against request json schema.

        If the request is not valid, the method publishes error message.

        @param request MQTTMessage object representing the published message
        @return ErrorCodes.OK if valid, ErrorCodes.SCHEMA_ERROR otherwise
        """
        topic_endpoint = self._extract_endpoint(request.topic)
        if re.fullmatch('(get|post|put|patch|delete)/.*', topic_endpoint) is None:
            error_string = 'Invalid request topic endpoint part: ' + request.topic
            self.logger.error(error_string)
            resp_topic = request.topic.replace('requests', 'responses')
            self.publish(resp_topic, error_string, 0, False)
            return ErrorCodes.TOPIC_ERROR

        if request.payload:
            try:
                payload = json.loads(request.payload.decode('utf-8'))
            except json.JSONDecodeError as json_error:
                self.logger.error(
                    json_error.msg + ' at line ' + str(json_error.lineno) +
                    ', column ' + str(json_error.colno) + ' on\n' + json_error.doc
                )
                resp_topic = request.topic.replace('requests', 'responses')
                self.publish(resp_topic, 'Invalid request format: ' +
                             json_error.msg + ' at line ' + str(json_error.lineno) +
                             ', column ' + str(json_error.colno) + ' on\n' + json_error.doc, 0, False)
                return ErrorCodes.JSON_ERROR

            schema_errors = sorted(self.validator.iter_errors(payload), key=str)
            if schema_errors:
                error_string = ''
                for error in schema_errors:
                    self.logger.error(error)
                    error_string += error.message
                resp_topic = request.topic.replace('requests', 'responses')
                self.publish(resp_topic, error_string, 0, False)
                return ErrorCodes.SCHEMA_ERROR

        return ErrorCodes.OK

    def on_connect(self, client, userdata, flags, rc):  # pylint: disable=W0221 W0236 W0613
        """
        Callback function for the MQTT connect method.

        Writes information about established connection to log file and subscribes to the MQTT topic.

        @param client: instance of cmqtt class for this callback
        @param userdata private user data of the client
        @param flags response flags from MQTT broker
        @param rc connection result
        """
        self.logger.info('Translator connected to %s:%s with result %s', str(self.addr), str(self.port), str(rc))
        self.subscribe(self.request_topic)

    def on_disconnect(self, client, userdata, rc):  # pylint: disable=W0221 W0236 W0613
        """
        Callback function for when MQTT disconnects.

        Logs disconnect information.
        @param client: instance of cmqtt class for this callback
        @param userdata private user data of the client
        @param rc connection result
        """
        if rc != 0:
            self.logger.error('Unexpected MQTT connection close. Reconnecting...')

    def on_message(self, client, userdata, msg):  # pylint: disable=W0221 W0236 W0613
        """
        Callback function listening on MQTT publish topic.

        Validates published request, builds REST API path and response topic.
        Publishes the result of REST API request execution.

        @param client instance of cmqtt class for this callback
        @param userdata private user data of the client
        @param msg MQTTMessage object representing the published message
        """
        self.logger.info('MQTT broker published on ' + msg.topic + ':\n' + msg.payload.decode('utf-8'))

        if self._validate_request(msg) == ErrorCodes.OK:
            self.endpoint = self._extract_endpoint(msg.topic)
            method = self.endpoint.split('/', 1)[0]
            uri = '/' + self.endpoint.split('/', 1)[1]

            payload = msg.payload.decode('utf-8')

            try:
                if method == 'delete':
                    response = self.rest_client.delete(uri)
                elif method == 'get':
                    response = self.rest_client.get(uri)
                elif method == 'post':
                    response = self.rest_client.post(uri, payload)
                elif method == 'put':
                    response = self.rest_client.put(uri, payload)
                elif method == 'patch':
                    response = self.rest_client.patch(uri, payload)
                else:
                    self.logger.error('Invalid request method: %s', method)
                    self.publish(
                        self.response_topic + '/' + self.endpoint,
                        'Invalid request method: ' + method,
                        0,
                        False
                    )
                    return

                self.publish(
                    self.response_topic + '/' + self.endpoint,
                    json.dumps(response.jsonify(), indent=2),
                    0,
                    False
                )
            except RequestException as conn_err:
                self.logger.error('Caught exception during request: %s', str(conn_err))
                self.publish(
                    self.response_topic + '/' + self.endpoint,
                    'Could not establish connection with the REST API,'
                    ' check logs for more details.',
                    0,
                    False
                )

    def on_publish(self, client, userdata, mid):  # pylint: disable=W0221 W0236 W0613
        """
        Callback function for mqtt.client publish method.

        Writes information about the published message to log file.

        @param client instance of cmqtt class for this callback
        @param userdata private user data of the client
        @param mid message ID
        """
        self.logger.info(
            'Published response on topic: %s with MID: %s',
            self.response_topic + '/' + self.endpoint, str(mid)
        )

    def on_subscribe(self, client, userdata, mid, qos):  # pylint: disable=W0221 W0236 W0613
        """
        Callback function for mqtt.client subscribe method.

        Writes information about the subscribed topic to log file.

        @param client instance of cmqtt class for this callback
        @param userdata private user data of the client
        @param mid message ID
        @param qos quality of service value
        """
        self.logger.info('Subscribed to topic: %s', self.request_topic)

    def setup(self, config, rest_client):
        """
        Initializes the cmqtt class instance.

        @param config dictionary containing translator configuration
        @param rest_client rest_client class instance
        """
        # initialize client variables
        self.addr = config['mqtt']['addr']
        self.port = config['mqtt']['port']
        self.rest_client = rest_client
        self.request_topic = config['mqtt']['request_topic'] + '+/#'
        self.response_topic = config['mqtt']['response_topic']
        self.username_pw_set(config['mqtt']['user'], config['mqtt']['pw'])
        self.enable_logger(logger=self.logger)

        try:
            # set up tls
            tls_config = config['mqtt']['tls']
            if tls_config['enabled']:
                self.logger.info('Configuring client for TLS.')
                self.tls_set(
                    ca_certs=self._check_tls_file(tls_config['trust_store']),
                    certfile=self._check_tls_file(tls_config['key_store']),
                    keyfile=self._check_tls_file(tls_config['private_key']),
                    cert_reqs=ssl.CERT_REQUIRED if tls_config['require_broker_certificate'] else ssl.CERT_OPTIONAL,
                    tls_version=ssl.PROTOCOL_TLS
                )

            # load schema message
            self.validator = CMqtt.load_schema()
            return ErrorCodes.OK
        except TlsFileNotFoundError:
            return ErrorCodes.IO_ERROR
        except IOError as io_error:
            self.logger.error('%s on file %s', io_error.strerror, io_error.filename)
            return ErrorCodes.IO_ERROR
        except json.JSONDecodeError as json_error:
            self.logger.error(
                json_error.msg + ' at line ' + str(json_error.lineno) +
                ', column ' + str(json_error.colno) + ' on\n' + json_error.doc
            )
            return ErrorCodes.JSON_ERROR
