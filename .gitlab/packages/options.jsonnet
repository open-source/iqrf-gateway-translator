{
  // Base Docker image
  baseImage: 'iqrftech/debian-python-builder',
  // Directory for package deployment
  deployDir: '/data/nginx/dl/iqrf-gateway-translator/${DIST}/${ARCH_PREFIX}${ARCH}/${STABILITY}/',
  // Commands for package build
  packageBuildCommands(distribution, stability): [
    'debuild -e CI_PIPELINE_ID -b -us -uc -tc',
  ],
}
