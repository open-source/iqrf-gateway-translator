"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from unittest.mock import MagicMock
import unittest
import requests
import responses
from translator.crest.rest_client import CRest

config = {
    "rest": {
        "api_key": "testapikey=",
        "addr": "localhost",
        "port": 8080,
    },
    "mqtt": {
        "cid": "test",
        "addr": "localhost",
        "port": 1883,
        "topic": "testtopic",
        "user": "testuser",
        "pw": "testpw"
    }
}

headers = {
    'Authorization': 'Bearer testapikey='
}

headers1 = headers.copy()
headers1['test-header'] = 'test-header-content'

requestdict = dict()
requestdict['1'] = '{"headers": {"Content-Type": "application/json"}, "body": {"test-data": "test-content"}}'
requestdict['2'] = '{"headers": {"Content-Type": "application/zip"}, "body": "dGVzdA=="}'
requestdict['3'] = '{"headers": {"Content-Type": "text/plain"}, "body": "test"}'


class TestCRest(unittest.TestCase):

    def setUp(self):
        self.rest_client = CRest(config)

    def test_build_url(self):
        url = 'http://localhost:8080/api/v0/test/endpoint'
        self.assertEqual(self.rest_client._build_url('/test/endpoint'), url)

    def test_build_headers_none(self):
        self.assertEqual(self.rest_client._build_headers(None), headers)

    def test_build_headers_all(self):
        hdict = {'test-header': 'test-header-content'}
        self.assertEqual(self.rest_client._build_headers(hdict), headers1)

    def test_info_payload_bin(self):
        mock = self.rest_client.logger
        mock.info = MagicMock()

        self.rest_client._info_payload('application/zip', b'\x74\x65\x73\x74')
        mock.info.assert_called_with('Received payload of %s bytes', '4')

    def test_info_payload_json(self):
        mock = self.rest_client.logger
        mock.info = MagicMock()

        self.rest_client._info_payload('application/json', '{"test-data": "test-content"}')
        mock.info.assert_called_with('Received payload:\n%s', '{\n  "test-data": "test-content"\n}')

    def test_info_payload_other(self):
        mock = self.rest_client.logger
        mock.info = MagicMock()

        self.rest_client._info_payload('text/plain', 'test')
        mock.info.assert_called_with('Received payload: %s', 'test')

    @responses.activate
    def test_extract_payload_json(self):
        responses.add(responses.GET, 'http://testurl.com/api/test/endpoint', json={'test-data': 'test-data-content'},
                      status=200)
        response = requests.get('http://testurl.com/api/test/endpoint')
        self.assertEqual(self.rest_client._extract_payload(response), '{"test-data": "test-data-content"}')

    @responses.activate
    def test_extract_payload_bin(self):
        responses.add(responses.GET, 'http://testurl.com/api/test/endpoint', body=b'\x74\x65\x73\x74',
                      content_type='application/zip', status=200)
        response = requests.get('http://testurl.com/api/test/endpoint')
        self.assertEqual(self.rest_client._extract_payload(response), "dGVzdA==")

    @responses.activate
    def test_extract_payload_text(self):
        responses.add(responses.GET, 'http://testurl.com/api/test/endpoint', body='test', status=200)
        response = requests.get('http://testurl.com/api/test/endpoint')
        self.assertEqual(self.rest_client._extract_payload(response), 'test')

    @responses.activate
    def test_extract_payload_none(self):
        responses.add(responses.GET, 'http://testurl.com/api/test/endpoint', body='test', status=200)
        response = requests.get('http://testurl.com/api/test/endpoint')
        response.headers.pop('Content-Type')
        self.assertEqual(self.rest_client._extract_payload(response), None)

    def test_info_status_success(self):
        mock = self.rest_client.logger
        mock.info = MagicMock()

        self.rest_client._info_status(200, 'GET')
        mock.info.assert_called_with('%s request successful with status code: %s', 'GET', str(200))

    def test_info_status_fail(self):
        mock = self.rest_client.logger
        mock.info = MagicMock()

        self.rest_client._info_status(404, 'GET')
        mock.info.assert_called_with('%s request unsuccessful with status code: %s', 'GET', str(404))

    def test_parse_request_json(self):
        self.assertEqual(self.rest_client._parse_request(requestdict['1']), ('{"test-data": "test-content"}',
                                                                             {"Content-Type": "application/json"}))

    def test_parse_request_bin(self):
        self.assertEqual(self.rest_client._parse_request(requestdict['2']), (b'\x74\x65\x73\x74',
                                                                             {"Content-Type": "application/zip"}))

    def test_parse_request_text(self):
        self.assertEqual(self.rest_client._parse_request(requestdict['3']), ('test', {"Content-Type": "text/plain"}))

    def test_parse_request_none(self):
        self.assertEqual(self.rest_client._parse_request(None), ('', None))


if __name__ == '__main__':
    unittest.main()
