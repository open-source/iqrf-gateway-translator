# IQRF Gateway Translator

Translation from MQTT to REST and vice versa.

# Installation and running

## Prerequisities
Install python 3 and required packages:

```shell script
apt-get install python3 python3-pip
pip3 install -r requirements.txt
```

## Translator configuration
Translator configuration can be changed [here](config/config.json).
Configure the translator before launching it.

## Run for development purposes
Clone the repository and use the following command:

```shell script
make run
```

## Using as python package
Translator python package can be installed using:

```shell script
make install
```

To uninstall the package, run:

```shell script
make uninstall
```

## Building a debian package
Install prerequisities to build the package:

```shell script
apt-get install -y devscripts build-essential lintian debhelper dh-python python3-all python3-setuptools
```

Build python and then debian packages

```shell script
make build
```

## Delete generated pre-package files and binaries
Also applies to installing as python package

```shell script
make clean
```

# Using the translator

The translator connects and subscribes to the MQTT broker and topic specified in the configuration file.
Once a message is published on the request topic, the message content is processed and a REST request is created.
Then a REST response is processed and published on the response topic of the MQTT broker.
MQTT topics contain request methods and REST API endpoints.

MQTT topics:
- requests: `gateway/{GWID}/rest/requests/{method}/{endpoint}`
- responses: `gateway/{GWID}/rest/responses/{method}/{endpoint}`

Supported methods
- GET
- POST
- PUT
- DELETE

## Request data format

The expected request format is a json string:

```json
{
  "headers": {
    "Content-Type": "some content type..."
  },
  "body": "some data..."
}
```

Headers are standard HTTP headers.

Accepted content types:

- application/json (string dump of a json object)
- application/zip (string containing base64 encoded binary data)
- text/plain
- text/html

## Response data format

The translator publishes response of the following json string format:

```json
{
  "headers": {
    "Content-Type": "some content type..."
  },
  "body": "some data...",
  "status": 200
}
```

Binary data will be sent in the response body as a string containing base64 encoded binary data.

## Example request data

JSON data:

```json
{
  "headers": {
    "Content-Type": "application/json"
  },
  "body": {"content": "data"}
}
```

Binary data:

```json
{
  "headers": {
    "Content-Type": "application/zip"
  },
  "body": "dGVzdA=="
}
```

## Acknowledgement

This project has been made possible with a government grant by means of [the Ministry of Industry and Trade of the Czech Republic](https://www.mpo.cz/) in [the TRIO program](https://starfos.tacr.cz/cs/project/FV40132).