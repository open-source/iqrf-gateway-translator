[
  { name: 'debian', version: 'buster', arch: 'all' },
  { name: 'debian', version: 'bullseye', arch: 'all' },
  { name: 'debian', version: 'bookworm', arch: 'all' },
  { name: 'debian', version: 'trixie', arch: 'all' },
  { name: 'ubuntu', version: 'bionic', arch: 'all' },
  { name: 'ubuntu', version: 'focal', arch: 'all' },
  { name: 'ubuntu', version: 'jammy', arch: 'all' },
  { name: 'ubuntu', version: 'noble', arch: 'all' },
]
