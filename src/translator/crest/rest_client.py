"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import base64
import json
import logging
import requests

from translator.utils.rest_response import RestResponse


class CRest:
    """
    A class implementing behaviour of the Translator's REST client.

    Attributes
    ----------
    addr: str
        REST API address
    api_key: str
        API key for the REST API
    logger: logger
        logger instance for the rest_client object
    port: int
        REST API port

    Methods
    -------
    buildUrl(path)
        Returns a URL string with REST API endpoint.
    buildHeaders(hlist)
        Returns a dictionary of headers for REST API request.
    extractPayload(response)
        Extracts payload from REST API response.
    infoStatus(status, method)
        Writes information about REST API request result to log file.
    parseRequest(request)
        Separates request headers and payload.
    delete(path)
        Creates DELETE method request, executes the request and returns RestResponse object.
    get(path)
        Creates GET method request, executes the request and returns RestResponse object.
    post(path, request)
        Creates POST method request, executes the request and returns RestResponse object.
    put(path, request)
        Creates PUT method request, executes the request and returns RestResponse object.
    """
    def __init__(self, config):
        """
        @param config dictionary containing the Translator's configuration
        """
        self.addr = config['rest']['addr']
        self.api_key = config['rest']['api_key']
        self.logger = logging.getLogger(__name__)
        self.port = config['rest']['port']

    def _build_url(self, path):
        """
        Returns a URL string with REST API endpoint.

        @param path REST API endpoint path
        @return request URL
        """
        return 'http://' + self.addr + ':' + str(self.port) + '/api/v0' + path

    def _build_headers(self, hlist):
        """
        Creates a dictionary of request headers to be sent to the REST API.

        @param hlist dictionary of request headers
        @return headers dictionary of request headers with authentication
        """
        headers = {
            'Authorization': 'Bearer ' + self.api_key
        }
        if hlist:
            for header in hlist:
                headers[header] = hlist[header]

        return headers

    def _info_payload(self, c_type, payload):
        """
        Writes info info about the REST API response payload to log file.

        @param c_type content type from REST API response headers
        @param payload REST API response payload
        """
        if c_type == 'application/zip' or 'text/html' in c_type:
            self.logger.info('Received payload of %s bytes', str(len(payload)))
        elif c_type == 'application/json':
            self.logger.info('Received payload:\n%s', json.dumps(json.loads(payload), indent=2))
        else:
            self.logger.info('Received payload: %s', payload)

    @staticmethod
    def _extract_payload(response):
        """
        Extracts payload from the REST API response and converts it to a JSON serializable format.

        @param response REST API response
        @return processed REST API response payload
        """
        payload = None

        if 'Content-Type' in response.headers:
            if response.headers['Content-Type'] == 'application/json':
                payload = json.dumps(response.json())
            elif response.headers['Content-Type'] == 'application/zip':
                payload = base64.b64encode(response.content).decode('utf-8')
            elif 'text/plain' in response.headers['Content-Type'] or 'text/html' in response.headers['Content-Type']:
                payload = response.text

        return payload

    def _info_status(self, status, method):
        """
        Writes information about REST API request result to log file.

        @param status REST API response status
        @param REST API
        """
        if 200 <= status < 300:
            self.logger.info('%s request successful with status code: %s', method, str(status))
        else:
            self.logger.info('%s request unsuccessful with status code: %s', method, str(status))

    @staticmethod
    def _parse_request(request):
        """
        Separates request headers and payload.

        @param request request published to MQTT publish topic
        @return payload of the request and request headers
        """
        if request:
            json_object = json.loads(request)

            payload = ''
            headers = json_object['headers']

            if 'Content-Type' in json_object['headers']:
                c_type = json_object['headers']['Content-Type']
                if c_type == 'application/json':
                    payload = json.dumps(json_object['body'])
                elif c_type == 'application/zip':
                    payload = base64.b64decode(json_object['body'])
                elif 'text/plain' in c_type or 'text/html' in c_type:
                    payload = json_object['body']
            return payload, headers
        return '', None

    def delete(self, path):
        """
        Creates DELETE method request to be executed, then executes the request
        and returns the result as a RestResponse object.

        @path REST API endpoint
        @return RestResponse object containing the response
        """
        self.logger.info('Sending DELETE request for: %s', self._build_url(path))
        response = requests.delete(self._build_url(path), headers=self._build_headers(None), timeout=90)

        self._info_status(response.status_code, 'DELETE')
        delete_payload = self._extract_payload(response)

        if delete_payload:
            self._info_payload(response.headers['Content-Type'], delete_payload)
        else:
            self.logger.info('DELETE response has no payload')

        response = RestResponse(response.headers, delete_payload, response.status_code)

        return response

    def get(self, path):
        """
        Creates GET method request to be executed, then executes the request
        and returns the result as a RestResponse object.

        @path REST API endpoint
        @return RestResponse object containing the response
        """
        self.logger.info('Sending GET request for: %s', self._build_url(path))
        response = requests.get(self._build_url(path), headers=self._build_headers(None), timeout=90)

        self._info_status(response.status_code, 'GET')
        get_payload = self._extract_payload(response)

        if get_payload:
            self._info_payload(response.headers['Content-Type'], get_payload)
        else:
            self.logger.info('GET response has no payload')

        response = RestResponse(response.headers, get_payload, response.status_code)

        return response

    def post(self, path, request):
        """
        Creates POST method request to be executed, then executes the request
        and returns the result as a RestResponse object.

        @path REST API endpoint
        @return RestResponse object containing the response
        """
        self.logger.info('Sending POST request for: %s', self._build_url(path))

        payload, headers = self._parse_request(request)

        if payload:
            self.logger.info('Payload of the POST request:\n%s', str(payload))
            response = requests.post(
                self._build_url(path),
                headers=self._build_headers(headers),
                data=payload,
                timeout=90,
            )
        else:
            self.logger.info('POST request has no payload')
            response = requests.post(self._build_url(path), headers=self._build_headers(headers), timeout=90)

        self._info_status(response.status_code, 'POST')
        post_payload = self._extract_payload(response)

        if post_payload:
            self._info_payload(response.headers['Content-Type'], post_payload)
        else:
            self.logger.info('POST response has no payload')

        response = RestResponse(response.headers, post_payload, response.status_code)

        return response

    def put(self, path, request):
        """
        Creates PUT method request to be executed, then executes the request
        and returns the result as a RestResponse object.

        @path REST API endpoint
        @return RestResponse object containing the response
        """
        self.logger.info('Sending PUT request for: %s', self._build_url(path))

        payload, headers = self._parse_request(request)

        if payload:
            self.logger.info('Payload of the PUT request:\n%s', str(payload))
            response = requests.put(
                self._build_url(path),
                headers=self._build_headers(headers),
                data=payload,
                timeout=90,
            )
        else:
            self.logger.info('PUT request has no payload')
            response = requests.put(self._build_url(path), headers=self._build_headers(headers), timeout=90)

        self._info_status(response.status_code, 'PUT')
        put_payload = self._extract_payload(response)

        if put_payload:
            self._info_payload(response.headers['Content-Type'], put_payload)
        else:
            self.logger.info('PUT response has no payload')

        response = RestResponse(response.headers, put_payload, response.status_code)

        return response

    def patch(self, path, request):
        """
        Creates PATCH method request to be executed, then executes the request
        and returns the result as a RestResponse object.

        @path REST API endpoint
        @return RestResponse object containing the response
        """
        self.logger.info('Sending PATCH request for: %s', self._build_url(path))

        payload, headers = self._parse_request(request)

        if payload:
            self.logger.info('Payload of the PATCH request:\n%s', str(payload))
            response = requests.patch(
                self._build_url(path),
                headers=self._build_headers(headers),
                data=payload,
                timeout=90,
            )
        else:
            self.logger.info('PATCH request has no payload')
            response = requests.patch(self._build_url(path), headers=self._build_headers(headers), timeout=90)

        self._info_status(response.status_code, 'PATCH')
        patch_payload = self._extract_payload(response)

        if patch_payload:
            self._info_payload(response.headers['Content-Type'], patch_payload)
        else:
            self.logger.info('PATCH response has no payload')

        response = RestResponse(response.headers, patch_payload, response.status_code)

        return response
