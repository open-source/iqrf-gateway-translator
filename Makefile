.PHONY: build test

build: clean
	python3 setup.py sdist
	debuild -b -tc
	mkdir build
	mv ../iqrf-gateway-translator_* build

clean:
	rm -rf build dist iqrf_gateway_translator.egg-info src/iqrf_gateway_translator.egg-info

install:
	python3 setup.py install --user

run:
	python3 src/translator/__main__.py -c config/config.json -l iqrf-gateway-translator.log

test:
	nosetests ./test -v --with-coverage --cover-package=translator --cover-inclusive --cover-erase --cover-html
	pycodestyle ./src/* --max-line-length=120
	pylint ./src/* --max-line-length=120

uninstall:
	pip3 uninstall iqrf-gateway-translator

