#!/usr/bin/env python3
"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import logging
import pathlib
import sys

from paho.mqtt.enums import CallbackAPIVersion

from translator import __version__
from translator.cmqtt.mqtt_client import CMqtt
from translator.crest.rest_client import CRest
from translator.utils.config import Config
from translator.utils.error_codes import ErrorCodes

path = pathlib.Path(__file__).parent


def main():  # pylint: disable=R1710
    """
    Main function of the Translator.
    Sets up logging, initializes REST and MQTT client instances and starts MQTT client loop.

    @return return code if an error occurs while loading configuration or initializing MQTT client.
    """
    # initialize argument parser
    parser = argparse.ArgumentParser(prog='IQRF Gateway Translator', description='MQTT to REST translator.')
    parser.add_argument('-c', '--config', type=str, dest='config',
                        help='Path to configuration file', required=True)
    parser.add_argument('-s', '--schema', type=str, dest='schema', default=path / 'utils/schema_config.json',
                        help='Path to jsonschema file', required=False)
    parser.add_argument('-l', '--logfile', type=str, dest='logfile', default='/var/log/iqrf-gateway-translator.log',
                        help='Path to log file', required=False)
    parser.add_argument('-v', '--version', action='version', version='%(prog)s v' + __version__)
    args = parser.parse_args()

    # set up logging
    logging.basicConfig(filename=args.logfile, filemode='w',
                        format='[%(asctime)s]: [%(name)s] - [%(levelname)s] - %(message)s (%(filename)s:%(lineno)s)',
                        datefmt='%d-%m-%y %H:%M:%S', level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info('IQRF Gateway Translator v%s', __version__)

    # read configuration
    ret, config = Config(args.config, args.schema).read()
    if ret != ErrorCodes.OK:
        logger.info('Service shutting down.')
        return 1

    # initialize rest client
    rest_client = CRest(config)

    # initialize mqtt client
    mqtt_client = CMqtt(
        callback_api_version=CallbackAPIVersion.VERSION1,
        client_id=config['mqtt']['cid'],
    )
    if mqtt_client.setup(config, rest_client) != ErrorCodes.OK:
        logger.info('Service shutting down.')
        return 1

    # establish connection to mqtt broker
    try:
        mqtt_client.connect(mqtt_client.addr, mqtt_client.port)
    except ConnectionRefusedError as conn_error:
        logger.error('%s [%s]', conn_error.strerror, str(conn_error.errno))
        return 1
    mqtt_client.loop_forever()


if __name__ == '__main__':
    sys.exit(main())
