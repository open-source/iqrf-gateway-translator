"""
Copyright 2020-2021 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import json
import pathlib
import unittest
import paho.mqtt.client as mqtt
from unittest.mock import Mock, patch

from paho.mqtt.enums import CallbackAPIVersion

from translator.cmqtt.mqtt_client import CMqtt
from translator.utils.error_codes import ErrorCodes
from translator.utils.exceptions import TlsFileNotFoundError

test_json = {
    "rest": {
        'api_key': 'testapikey=',
        'addr': 'localhost',
        'port': 8080,
    },
    "mqtt": {
        'cid': 'test',
        'addr': 'localhost',
        'port': 1883,
        'request_topic': 'reqtopic/',
        'response_topic': 'resptopic/',
        'user': '',
        'pw': '',
        'tls': {
            'enabled': False,
            'trust_store': '',
            'key_store': '',
            'private_key': '',
            'require_broker_certificate': True
        }
    }
}

class TestCMqtt(unittest.TestCase):

    def setUp(self):
        self.config = test_json
        self.mqtt_client = CMqtt(
            callback_api_version=CallbackAPIVersion.VERSION1,
            client_id='Test client',
        )
        self.mqtt_client.setup(self.config, None)
        self.request = Mock(spec=mqtt.MQTTMessage)
        self.request.topic = 'reqtopic/post/test'
        self.cert = pathlib.Path(__file__).parent / 'data/example.crt'

    @staticmethod
    def read(path):
        with open(path, 'r') as file:
            data = file.read()
        return data

    def test_setup_ok(self):
        self.assertEqual(self.mqtt_client.setup(self.config, None), ErrorCodes.OK)

    def test_setup_tls_ok(self):
        self.config['mqtt']['tls']['enabled'] = True
        self.config['mqtt']['tls']['trust_store'] = self.cert
        self.assertEqual(self.mqtt_client.setup(self.config, None), ErrorCodes.OK)

    def test_setup_tls_error(self):
        self.config['mqtt']['tls']['enabled'] = True
        self.config['mqtt']['tls']['trust_store'] = 'nonexistent.crt'
        self.assertEqual(self.mqtt_client.setup(self.config, None), ErrorCodes.IO_ERROR)

    def test_check_tls_file_none(self):
        self.assertIsNone(self.mqtt_client._check_tls_file(''))
    
    def test_check_tls_file_nonexistent(self):
        with self.assertRaises(TlsFileNotFoundError):
            self.mqtt_client._check_tls_file('nonexistent.crt')
    
    def test_check_tls_file_ok(self):
        self.assertEqual(self.mqtt_client._check_tls_file(self.cert), self.cert)

    def test_validate_request_none(self):
        self.request.payload = None
        self.assertEqual(self.mqtt_client._validate_request(self.request), ErrorCodes.OK)

    def test_validate_request_ok(self):
        self.request.payload = b'\x7b\x22\x68\x65\x61\x64\x65\x72\x73\x22\x3a\x20\x7b\x22\x43\x6f\x6e\x74\x65\x6e\x74' \
                               b'\x2d\x54\x79\x70\x65\x22\x3a\x20\x22\x74\x65\x78\x74\x2f\x70\x6c\x61\x69\x6e\x22\x7d' \
                               b'\x2c\x20\x22\x62\x6f\x64\x79\x22\x3a\x20\x22\x74\x65\x73\x74\x22\x7d'
        self.assertEqual(self.mqtt_client._validate_request(self.request), ErrorCodes.OK)

    def test_validate_request_topic_error(self):
        self.request.payload = None
        self.request.topic = 'reqtopic/post'
        self.assertEqual(self.mqtt_client._validate_request(self.request), ErrorCodes.TOPIC_ERROR)

    def test_validate_request_json_error(self):
        self.request.payload = b'\x7b\x22\x74\x65\x73\x74\x2c\x7d'
        self.assertEqual(self.mqtt_client._validate_request(self.request), ErrorCodes.JSON_ERROR)

    def test_validate_request_schema_error(self):
        self.request.payload = b'\x7b\x22\x68\x65\x61\x64\x65\x72\x73\x22\x3a\x20\x7b\x22' \
                               b'\x43\x6f\x6e\x74\x65\x6e\x74\x2d\x54\x79\x70\x65\x22\x3a' \
                               b'\x20\x22\x74\x65\x78\x74\x2f\x70\x6c\x61\x69\x6e\x22\x7d\x7d'
        self.assertEqual(self.mqtt_client._validate_request(self.request), ErrorCodes.SCHEMA_ERROR)

if __name__ == '__main__':
    unittest.main()
